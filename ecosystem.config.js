module.exports = {
  apps : [
      {
        name: "api-nodejs-scaffolding",
        script: "./app.js",
        watch: true,
        env: {
          "NODE_ENV": "dev"
        },
        env_prod: {
          "NODE_ENV": "prod",
        }
      }
  ]
}