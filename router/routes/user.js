var express = require('express');
var router = express.Router();
var usuarioController = include('./controller/user');

/**
 * @api {post} /api/auth JWT generation
 * @apiVersion 0.0.1
 * @apiName Auth
 * @apiGroup User
 * 
 * @apiParam {String} [username] User username
 * @apiParam {String} [password] User password
 *
 * @apiSampleRequest /api/auth
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *  "message": "Sucessfull generated token",
 *  "username": "user",
 *  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImZmIiwiaWF0IjoxNTAzNTA2NDY2LCJleHAiOjE1MDM1MDY1ODZ9.PTRkagtUYe3pPm_ceM7rDgAFhtFFoTGBBfui1zWM180",
 *  "token_exp": 1503506586,
 *  "token_iat": 1503506466
 * }
 *
 * @apiError (Error 500) ErrorInterno Ocurrió un error interno en el servidor.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       code: 500,
 *       error: "Internal Server Error"
 *     }
 *
 * @apiError (Error 401) Unauthorized Error.
 *
 * @apiErrorExample Username/password-vacios:
 *     HTTP/1.1 401: Unauthorized
 *     {
 *      "code": 401,
 *      "error": "Unauthorized Error"
 *     }
 *
 * @apiErrorExample Bad request:
 *     HTTP/1.1 400: Bad-Request
 *     {
 *      "code": 400,
 *      "error": "Bad-Request"
 *     }
 *
 */
router.post('/auth', usuarioController.authenticate);

/**
 * @api {post} /api/auth/refresh Refresh JWT token
 * @apiVersion 0.0.1
 * @apiName JWTTokenRefresh
 * @apiGroup User
 *
 * @apiSampleRequest /api/auth/refresh
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *  "message": "Sucessfull generated token",
 *  "username": "user",
 *  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImZmIiwiaWF0IjoxNTAzNTA2NDY2LCJleHAiOjE1MDM1MDY1ODZ9.PTRkagtUYe3pPm_ceM7rDgAFhtFFoTGBBfui1zWM180",
 *  "token_exp": 1503506586,
 *  "token_iat": 1503506466
 * }
 *
 * @apiError (Error 500) Internal Server Error
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       code: 500,
 *       error: "Internal Server Error"
 *     }
 *
 * @apiErrorExample Unauthorized-Response:
 *     HTTP/1.1 401: Unauthorized
 *     {
 *      "code": 401,
 *      "error": "Unauthorized error"
 *     }
 *
 */
router.post('/auth/refresh', usuarioController.refresh);

module.exports = router;