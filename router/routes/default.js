var express = require('express');
var router = express.Router();
var defaultController = include('./controller/index');

/**
 * @api {get} /api/ Default
 * @apiVersion 0.0.1
 * @apiName Default
 * @apiGroup Default
 * 
 * @apiSampleRequest /api/
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *  name: "API NODEJS Scaffolding",
 *  version: "0.0.1"
 * }
 */
router.get('/', defaultController.index);

module.exports = router;