// *** absolute path configuration *** //
global.base_dir = __dirname+'/..';
global.abs_path = function(path) {
  return base_dir + path;
}
global.include = function(file) {
  return require(abs_path('/' + file));
}

// *** config file *** //
const path = require('path');
const enviroment = (process.env.NODE_ENV)? process.env.NODE_ENV:'dev';

var config = require(path.join(__dirname,'.','config.'+enviroment+'.json'));

module.exports = config;