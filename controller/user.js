var orm = include('lib/orm/mysql');

const common = include("controller/common");
const config = include('config/config');

var _ = require("underscore");
var token = include("utils/token");

exports.authenticate = function(req, res, next){

	var username = req.body.username,
        password = req.body.password;

    if (_.isEmpty(username) || _.isEmpty(password)) {
        return common.responseUnauthorizedAccess(res,"Invalid username and/or password");
    }

    var t = token.create({
		username: username
	});

    return common.responseSucessfull(res,Object.assign({message: "Sucessfull generated token"},t));

};

exports.refresh = function(req, res, next){

    var bearer = req.headers.authorization.split(" "),
        bearerToken = bearer[1],
        data = token.decode(bearerToken);
    
    if(!data){
    	return common.responseUnauthorizedAccess(res,"Token doesn't exists");
    }

    var t = token.verify(bearerToken);

	if(t){
		return common.responseSucessfull(res,Object.assign({message: "Sucessfull generated token"},t));
	} else{
		return common.responseUnauthorizedAccess(res,"Token could not be verified");
	}

};