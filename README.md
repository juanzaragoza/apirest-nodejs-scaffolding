# API NodeJS Scaffolding

Estructura de directorios basica para iniciar un proyecto en Node.js y Express.js. 
El proyecto contiene

* Codigo autodocumentado con apiDoc
* Autenticación y seguridad con JWT
* ORM basico Sequelize

## Requerimientos

* NodeJS v6.8.1
* grunt-cli@1.2.0
* PM2@3.10.10

## Instalación

Instalar Grunt y PM2

		npm install -g grunt-cli
		npm install -g pm2

Ejecutar en el raiz del proyecto

		npm install

Copiar el archivo ´config/config.default.json´ a ´config/config.{env}.json´ y configurarlo.

Generar el apidoc

		./node_modules/apidoc/bin/apidoc -i router/ -o apidoc/

## Levantar el servidor

En caso de existir otras aplicaciones corriendo con PM2, verificar si existe un archivo de configuración ´ecosystem.json´ y agregar el siguiente elemento al array ´apps´ con la siguiente información:

		{
	  		name: "api-m4",
		  	script: "./app.js",
		  	watch: true,
		  	env: {
		    	"NODE_ENV": "dev"
		  	},
		  	env_prod: {
		    	"NODE_ENV": "prod",
		  	}
		}

En caso de no existir, ejecutar la aplicación usando pm2:

		pm2 start ecosystem.config.js --env prod
		pm2 save

En caso de necesitar reiniciarlo, ejecutar:

		pm2 reload ecosystem.config.js --update-env

## Pruebas

Desde un navegador, ingrese a la home de la aplicacion junto con el puerto configurado en ´config/config.json´. Por ejemplo: http://localhost:PORT/apidoc/




